package net.wishfull_thinking.ezshopper.view;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.TextView;

import net.wishfull_thinking.ezshopper.Utils;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by dennis on 8/30/17.
 * Yo, validate my sh**
 */

public class ItemValidator<T> extends TextValidator implements View.OnFocusChangeListener {

    private T mItem;
    private TextInputLayout mLayout;
    private final String mSetter;
    private boolean mRequired;

    public ItemValidator(TextView textView, T item, String setter) {
        super(textView);
        mItem = item;
        mSetter = setter;
        mRequired = true;
    }

    public ItemValidator(TextView textView, T item, String setter, boolean required) {
        this(textView, item, setter);

        mRequired = required;
    }

    public ItemValidator(TextView textView, T item, String setter, boolean required, TextInputLayout inputLayout) {
        this(textView, item, setter, required);
        mLayout = inputLayout;
        mLayout.setErrorEnabled(true);
    }

    @Override
    protected void validate(TextView textView) {

        String text = textView.getText().toString();
        if (mRequired && Utils.isBlank(text)) {
            if (mLayout != null) {
                mLayout.setError("This is required!");
            } else {
                textView.setError("This is required!");     // TODO: Stringify this into resources
            }
        } else {
            if (mLayout != null) {
                mLayout.setError(null);
            } else {
                textView.setError(null);
            }
            try {
                mItem.getClass().getMethod(mSetter, String.class).invoke(mItem, text);
            }
            catch (NoSuchMethodException ignored) { }
            catch (IllegalAccessException ignored) { }
            catch (InvocationTargetException ignored) { }
        }
    }

    public void onFocusChange(View textView, boolean hasFocus) {

        if (!hasFocus)
            validate((TextView) textView);
    }

    public void setItem(T item) {
        mItem = item;
    }
}
