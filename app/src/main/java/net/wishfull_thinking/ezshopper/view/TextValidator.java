package net.wishfull_thinking.ezshopper.view;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by dennis on 8/30/17.
 * Validates stuff
 */

abstract class TextValidator implements TextWatcher {
    private final TextView textView;

    TextValidator(TextView textView) {
        this.textView = textView;
    }

    protected abstract void validate(TextView textView);

    @Override
    final public void afterTextChanged(Editable s) {
        validate(textView);
    }

    @Override
    final public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

    @Override
    final public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
}