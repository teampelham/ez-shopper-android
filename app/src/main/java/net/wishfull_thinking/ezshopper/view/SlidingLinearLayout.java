package net.wishfull_thinking.ezshopper.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by dennis on 9/4/17. Cuz this ObjectAnimator business just ain't it
 */

@SuppressWarnings("unused")
public class SlidingLinearLayout extends LinearLayout {

    public SlidingLinearLayout(Context context) {
        super(context);
    }

    public SlidingLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SlidingLinearLayout(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
    }

    public void setXFraction(final float fraction) {
        float translationX = getWidth() * fraction;
        setTranslationX(translationX);
    }

    public float getXFraction() {
        if (getWidth() == 0)
            return 0;

        return getTranslationX() / getWidth();
    }

    public void setYFraction(final float fraction) {
        float translationY = getHeight() * fraction;
        setTranslationY(translationY);
    }

    public float getYFraction() {
        if (getHeight() == 0)
            return 0;

        return getTranslationY() / getHeight();
    }
}
