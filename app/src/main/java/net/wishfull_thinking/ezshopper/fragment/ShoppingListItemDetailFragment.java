package net.wishfull_thinking.ezshopper.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;

import net.wishfull_thinking.ezshopper.EZShopperApplication;
import net.wishfull_thinking.ezshopper.R;
import net.wishfull_thinking.ezshopper.adapter.ShoppingListItem;

import butterknife.BindView;

/**
 * Created by dennis on 5/28/17.
 * Ah nuggets
 */

@SuppressWarnings("WeakerAccess")
public class ShoppingListItemDetailFragment extends BaseFragment {

    @BindView(R.id.product_name)
    AutoCompleteTextView mProductNameEdit;

    @BindView(R.id.product_quantity)
    EditText mProductQuantityEdit;

    @BindView(R.id.product_quantity_type)
    AutoCompleteTextView mQuantityTypeEdit;

    @BindView(R.id.product_category)
    AutoCompleteTextView mProductCategoryEdit;

    @BindView(R.id.list_spinner)
    AppCompatSpinner mListSpinner;

    private ShoppingListItem mItem;
    private String mItemID;
    private String mListID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mItem = new ShoppingListItem();

        Bundle bundle = savedInstanceState != null ? savedInstanceState : getArguments();
        if (bundle == null) {
            return;
        }

        String listID = bundle.getString(ShoppingListDetailFragment.ARG_LIST_ID);
        String itemID = bundle.getString(ShoppingListDetailFragment.ARG_ITEM_ID, null);

        setShoppingListItemId(listID, itemID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mTwoPane) {
            return super.onCreateView(inflater, container, savedInstanceState);
        } else {
            mRootView = inflater.inflate(R.layout.fragment_shoppinglist_item_detail, container, false);

            return mRootView;
        }
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstance) {
        if (mTwoPane) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            mRootView = inflater.inflate(R.layout.fragment_shoppinglist_item_detail, null);

            builder.setView(mRootView)
                    .setPositiveButton(R.string.dialog_save, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ShoppingListItemDetailFragment.this.getDialog().dismiss();
                        }
                    })
                    .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mItem.resetChanges();
                            ShoppingListItemDetailFragment.this.getDialog().cancel();
                        }
                    });

            return builder.create();
        } else {
            return super.onCreateDialog(savedInstance);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setupListSpinner();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (!(mItem.hasChanges() && mItem.isValid())) {
            return;
        }

        //String listId = mApplication.getShoppingListId(mListSpinner.getSelectedItem().toString());  // TODO: Fix null reference error

        CollectionReference listRef = mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                .collection(EZShopperApplication.SHOPPING_LISTS).document(mListID).collection(EZShopperApplication.SHOPPING_LIST_ITEMS);

        if (mItemID != null) {
            listRef.document(mItemID).set(mItem);
        } else {
            listRef.add(mItem);
        }

        getEZApplication().updateListData(mItem.getName(), mItem.getCategory(), mItem.getQuantityType());
    }

    public void setShoppingListItemId(String shoppingListId, String shoppingListItemId) {
        mItemID = shoppingListItemId;
        mListID = shoppingListId;

        if (mItemID == null) {
            return;
        }

        mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                .collection(EZShopperApplication.SHOPPING_LISTS).document(shoppingListId).collection(EZShopperApplication.SHOPPING_LIST_ITEMS)
                .document(shoppingListItemId).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            mItem = document.toObject(ShoppingListItem.class);

                            enableValidation();
                            mItem.resetChanges();
                            mProductNameEdit.setText(mItem.getName());
                            mProductQuantityEdit.setText(mItem.getQuantity());
                            mQuantityTypeEdit.setText(mItem.getQuantityType());
                            mProductCategoryEdit.setText(mItem.getCategory());
                            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_activity_edit);
                        }
                    }
                });
    }

    @Override
    protected void enableValidation() {
        EZShopperApplication application = getEZApplication();
        updateValidation(mItem, mProductNameEdit, application.getProductsAdapter(), "setName", true, (TextInputLayout)mRootView.findViewById(R.id.input_product_name));
        updateValidation(mItem, mProductCategoryEdit, application.getCategoriesAdapter(), "setCategory", false, (TextInputLayout)mRootView.findViewById(R.id.input_product_category));
        updateValidation(mItem, mProductQuantityEdit, null, "setQuantity", false, (TextInputLayout)mRootView.findViewById(R.id.input_product_quantity));
        updateValidation(mItem, mQuantityTypeEdit, application.getQuantityTypesAdapter(), "setQuantityType", false, (TextInputLayout)mRootView.findViewById(R.id.input_product_quantity_type));
    }

    private void setupListSpinner() {
        ArrayAdapter<String> adapter = getEZApplication().getShoppingListAdapter();
        mListSpinner.setAdapter(adapter);

        if (mListID != null) {
            int spinnerPosition = adapter.getPosition(getEZApplication().getShoppingListName(mListID));
            mListSpinner.setSelection(spinnerPosition);
        }
    }
}
