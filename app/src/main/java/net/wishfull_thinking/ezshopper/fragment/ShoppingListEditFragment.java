package net.wishfull_thinking.ezshopper.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;

import net.wishfull_thinking.ezshopper.EZShopperApplication;
import net.wishfull_thinking.ezshopper.R;
import net.wishfull_thinking.ezshopper.adapter.ShoppingList;

import butterknife.BindView;


/**
 * Created by dennis on 10/16/17.
 * Blue cheese!
 */

@SuppressWarnings("WeakerAccess")
public class ShoppingListEditFragment extends BaseFragment {

    private ShoppingList mList;
    private String mListId;

    @BindView(R.id.list_name)
    EditText mListNameEdit;

    @BindView(R.id.list_descr)
    EditText mListDescrEdit;

    @BindView(R.id.input_list_name)
    TextInputLayout mListNameInput;

    @BindView(R.id.input_list_descr)
    TextInputLayout mListDescrInput;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mList = new ShoppingList();

        Bundle bundle = savedInstanceState != null ? savedInstanceState : getArguments();
        if (bundle != null && bundle.containsKey(ShoppingListDetailFragment.ARG_LIST_ID)) {
            setShoppingListId(bundle.getString(ShoppingListDetailFragment.ARG_LIST_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mTwoPane) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }

        mRootView = inflater.inflate(R.layout.fragment_shoppinglist_edit, container, false);

        return mRootView;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstance) {
        if (!mTwoPane) {
            return super.onCreateDialog(savedInstance);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        mRootView = inflater.inflate(R.layout.fragment_shoppinglist_edit, null);

        builder.setView(mRootView)
                .setPositiveButton(R.string.dialog_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ShoppingListEditFragment.this.getDialog().dismiss();
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mList.resetChanges();
                        ShoppingListEditFragment.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (!(mList.hasChanges() && mList.isValid())) {
            return;
        }

        CollectionReference listRef = mFirestore.collection(EZShopperApplication.USERS)
                .document(mUser.getUid())
                .collection(EZShopperApplication.SHOPPING_LISTS);

        if (mListId != null) {
            listRef.document(mListId).set(mList);
        } else {
            listRef.add(mList);
        }
    }

    @Override
    protected void enableValidation() {
        updateValidation(mList, mListNameEdit, null, "setName", true, mListNameInput);
        updateValidation(mList, mListDescrEdit, null, "setDescr", false, mListDescrInput);
    }

    public void setShoppingListId(String shoppingListId) {
        mListId = shoppingListId;
        mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                .collection(EZShopperApplication.SHOPPING_LISTS).document(shoppingListId).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    mList = document.toObject(ShoppingList.class);

                    enableValidation();
                    mList.resetChanges();
                    mListNameEdit.setText(mList.getName());
                    mListDescrEdit.setText(mList.getDescr());
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_activity_edit_list);

                }
            }
        });
    }

    public void deleteList() {
        if (mListId == null) {
            return;
        }

        new AlertDialog.Builder(getActivity())
            .setMessage(R.string.delete_list_confirmation)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {
                    mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                            .collection(EZShopperApplication.SHOPPING_LISTS).document(mListId).delete();
                }})
            .setNegativeButton(android.R.string.no, null).show();
    }
}
