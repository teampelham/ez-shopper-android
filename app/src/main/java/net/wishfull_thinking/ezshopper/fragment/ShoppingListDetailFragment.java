package net.wishfull_thinking.ezshopper.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.WriteBatch;

import net.wishfull_thinking.ezshopper.EZShopperApplication;
import net.wishfull_thinking.ezshopper.R;
import net.wishfull_thinking.ezshopper.activity.BaseActivity;
import net.wishfull_thinking.ezshopper.activity.ShoppingListDetailActivity;
import net.wishfull_thinking.ezshopper.activity.ShoppingListItemDetailActivity;
import net.wishfull_thinking.ezshopper.activity.ShoppingListListActivity;
import net.wishfull_thinking.ezshopper.adapter.ShoppingListItemAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A fragment representing a single ShoppingList detail screen.
 * This fragment is either contained in a {@link ShoppingListListActivity}
 * in two-pane mode (on tablets) or a {@link ShoppingListDetailActivity}
 * on handsets.
 */
@SuppressWarnings("WeakerAccess")
public class ShoppingListDetailFragment extends Fragment
        implements ShoppingListItemAdapter.OnShoppingListItemSelectedListener {

    public static final String ARG_LIST_ID = "list_id";
    public static final String ARG_ITEM_ID = "item_id";

    private static final String TAG_ADD_ITEM = "add_shopping_list_item";
    private static final String TAG_EDIT_ITEM = "edit_shopping_list_item";

    private final List<String> mDoneItems = new ArrayList<>();
    private DocumentSnapshot mListSnapshot;
    private FirebaseFirestore mFirestore;
    private FirebaseUser mUser;
    private ShoppingListItemAdapter mAdapter;
    private String mSortBy = EZShopperApplication.LIST_NAME;
    private String mListId;
    private boolean mShowDone;

    @BindView(R.id.shoppinglist_detail)
    RecyclerView mShoppingListsRecycler;

    @BindView(R.id.view_empty)
    ViewGroup mEmptyView;

    @Nullable
    @BindView(R.id.fab)
    FloatingActionButton mFab;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ShoppingListDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Firestore
        mFirestore = FirebaseFirestore.getInstance();
        mUser = FirebaseAuth.getInstance().getCurrentUser();

        Bundle bundle = savedInstanceState != null ? savedInstanceState : getArguments();
        if (bundle == null || !bundle.containsKey(ARG_LIST_ID)) {
            return;
        }

        mListId = bundle.getString(ARG_LIST_ID);
        mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                .collection(EZShopperApplication.SHOPPING_LISTS).document(mListId).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {
                    mListSnapshot = task.getResult();

                    AppCompatActivity activity = (AppCompatActivity) ShoppingListDetailFragment.this.getActivity();
                    if (activity != null) {
                        activity.getSupportActionBar().setTitle(mListSnapshot.getString(EZShopperApplication.LIST_NAME));
                    }
                }
            }
        });

        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        if (fab != null) {
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (((EZShopperApplication)getActivity().getApplication()).getTwoPane()) {
                        ShoppingListItemDetailFragment fragment = new ShoppingListItemDetailFragment();
                        fragment.setShoppingListItemId(mListId, null);
                        fragment.show(((BaseActivity)getActivity()).getSupportFragmentManager(), TAG_ADD_ITEM);
                    } else {
                        Intent intent = new Intent(getActivity(), ShoppingListItemDetailActivity.class);
                        intent.putExtra(ARG_LIST_ID, mListId);
                        getActivity().startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (mListId != null) {
            savedInstanceState.putString(ARG_LIST_ID, mListId);
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.shoppinglist_detail, container, false);

        ButterKnife.bind(this, rootView);

        if (mListId != null) {
            setupRecyclerView();
        }

        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.item_detail, menu);

        Drawable icon = menu.findItem(R.id.action_sort).getIcon();

        if (icon != null) {
            icon.mutate();
            icon.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_show_done:
                mShowDone = !mShowDone;
                mAdapter.notifyDataSetChanged();
                return true;
            case R.id.action_sort:
                mSortBy = mSortBy.equals(EZShopperApplication.LIST_NAME) ? EZShopperApplication.CATEGORY : EZShopperApplication.LIST_NAME;
                mAdapter.setQuery(getQuery());
                return true;
            case R.id.action_delete_done:
                deleteDoneItems();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteDoneItems() {
        Executor executor = Executors.newSingleThreadExecutor();
        Tasks.call(executor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                // Get the first batch of documents in the collection
                Query query = mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                        .collection(EZShopperApplication.SHOPPING_LISTS).document(mListId)
                        .collection(EZShopperApplication.SHOPPING_LIST_ITEMS)
                        .whereEqualTo(EZShopperApplication.DONE, true);

                // Get a list of deleted documents
                deleteQueryBatch(query);

                return null;
            }
        });
    }

    @WorkerThread
    private void deleteQueryBatch(final Query query) throws Exception {
        QuerySnapshot querySnapshot = Tasks.await(query.get());

        WriteBatch batch = query.getFirestore().batch();
        for (DocumentSnapshot snapshot : querySnapshot) {
            batch.delete(snapshot.getReference());
        }
        Tasks.await(batch.commit());

        querySnapshot.getDocuments();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mAdapter != null) {
            mAdapter.startListening();
        }
        runLayoutAnimation();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    private void setupRecyclerView() {
        //RecyclerView recyclerView = (RecyclerView)rootView.findViewById(R.id.shoppinglist_detail);
        //assert recyclerView != null;
        assert mUser != null;

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        mShoppingListsRecycler.setLayoutAnimation(animation);
        mAdapter = new ShoppingListItemAdapter(getActivity(), getQuery(), this) {
            @Override
            protected void onDataChanged() {
                // Show/hide content if the query returns empty
                if (getItemCount() == 0) {
                    mShoppingListsRecycler.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                } else {
                    mShoppingListsRecycler.setVisibility(View.VISIBLE);
                    mEmptyView.setVisibility(View.GONE);
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {
                // Show a snackbar for errors
                Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: check logs for info", Snackbar.LENGTH_LONG).show();
            }
        };
        mShoppingListsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        mShoppingListsRecycler.setAdapter(mAdapter);
    }

    private Query getQuery() {
        return mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
            .collection(EZShopperApplication.SHOPPING_LISTS).document(mListId)
            .collection(EZShopperApplication.SHOPPING_LIST_ITEMS)
            //.whereEqualTo("done", false)
            .orderBy(EZShopperApplication.DONE)
            .orderBy(mSortBy);
    }

    private void runLayoutAnimation() {
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(mShoppingListsRecycler.getContext(), R.anim.layout_animation_fall_down);
        mShoppingListsRecycler.setLayoutAnimation(animation);
        mAdapter.notifyDataSetChanged();
        mShoppingListsRecycler.scheduleLayoutAnimation();
    }

    @Override
    public void onShoppingListItemDone(DocumentSnapshot shoppingListItem, View view) {
        AppCompatCheckBox checkBox = (AppCompatCheckBox) view;

        Map<String, Object> data = new HashMap<>();
        data.put(EZShopperApplication.DONE, checkBox.isChecked());

        mDoneItems.add(shoppingListItem.getId());
        mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                .collection(EZShopperApplication.SHOPPING_LISTS).document(mListId)
                .collection(EZShopperApplication.SHOPPING_LIST_ITEMS)
                .document(shoppingListItem.getId()).set(data, SetOptions.merge());
    }

    @Override
    public void onShoppingListItemSelected(DocumentSnapshot shoppingListItem) {
        if (getActivity().getClass() == ShoppingListListActivity.class)
        {
            Bundle arguments = new Bundle();
            arguments.putString(ShoppingListDetailFragment.ARG_LIST_ID, mListId);
            arguments.putString(ShoppingListDetailFragment.ARG_ITEM_ID, shoppingListItem.getId());

            ShoppingListItemDetailFragment fragment = new ShoppingListItemDetailFragment();
            fragment.setArguments(arguments);
            fragment.show(((BaseActivity)getActivity()).getSupportFragmentManager(), TAG_EDIT_ITEM);
        }
        else {
            Intent intent = new Intent(getActivity(), ShoppingListItemDetailActivity.class);
            intent.putExtra(ARG_ITEM_ID, shoppingListItem.getId());
            intent.putExtra(ARG_LIST_ID, mListId);
            getActivity().startActivity(intent);
        }
    }

    @Override
    public boolean shouldShowDoneItem(DocumentSnapshot snapshot) {
        return mShowDone ||mDoneItems.contains(snapshot.getId());
    }
}
