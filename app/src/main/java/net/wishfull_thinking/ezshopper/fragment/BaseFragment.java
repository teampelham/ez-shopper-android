package net.wishfull_thinking.ezshopper.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import net.wishfull_thinking.ezshopper.EZShopperApplication;
import net.wishfull_thinking.ezshopper.view.ItemValidator;

import butterknife.ButterKnife;

/**
 * Created by dennis on 11/14/17.
 * BASE!
 */

public abstract class BaseFragment extends DialogFragment {

    FirebaseFirestore mFirestore;
    FirebaseUser mUser;
    View mRootView;
    boolean mTwoPane;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirestore = FirebaseFirestore.getInstance();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mTwoPane = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    @Override
    public void onStart() {
        super.onStart();

        ButterKnife.bind(this, mRootView);
        enableValidation();
    }

    protected abstract void enableValidation();


    @SuppressWarnings("unchecked")
    <T> void updateValidation(T item, EditText editText, ArrayAdapter<String> adapter, String setter, boolean required, TextInputLayout textInputLayout) {
        View.OnFocusChangeListener listener = editText.getOnFocusChangeListener();
        ItemValidator<T> validator;

        if (listener != null && ItemValidator.class.isInstance(listener)) {
            validator = (ItemValidator<T>) listener;
            validator.setItem(item);
        } else {
            validator = new ItemValidator<>(editText, item, setter, required, textInputLayout);
            editText.addTextChangedListener(validator);
            editText.setOnFocusChangeListener(validator);
        }

        if (adapter != null) {
            ((AutoCompleteTextView)editText).setAdapter(adapter);
        }
    }

    EZShopperApplication getEZApplication() {
        return (EZShopperApplication)getActivity().getApplication();
    }
}
