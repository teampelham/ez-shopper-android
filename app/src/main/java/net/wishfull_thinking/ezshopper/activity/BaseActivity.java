package net.wishfull_thinking.ezshopper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


import butterknife.ButterKnife;
import butterknife.BindView;

import net.wishfull_thinking.ezshopper.EZShopperApplication;
import net.wishfull_thinking.ezshopper.R;

/**
 * Created by dennis on 6/7/17.
 * Cuz I'm special
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        bindViews();
    }

    private void bindViews() {
        ButterKnife.bind(this);
        setupToolbar();
    }

    private void onLeaveActivity() {
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    private void onStartActivity() {
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
    }

    @Override
    public void finish() {
        super.finish();
        onLeaveActivity();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onLeaveActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(menuItem.getItemId() == android.R.id.home) {
            onLeaveActivity();
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        onStartActivity();
    }

    @Override
    public void startActivity(Intent intent, Bundle options) {
        super.startActivity(intent, options);
        onStartActivity();
    }

    private void setupToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    void showUpAsHome() {
        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    EZShopperApplication getEZApplication() {
        return (EZShopperApplication) getApplication();
    }
}
