package net.wishfull_thinking.ezshopper.activity;

import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import net.wishfull_thinking.ezshopper.R;
import net.wishfull_thinking.ezshopper.fragment.ShoppingListDetailFragment;
import net.wishfull_thinking.ezshopper.fragment.ShoppingListEditFragment;

public class ShoppingListEditActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list_edit);

        showUpAsHome();
        if (mToolbar != null) {
            mToolbar.setTitle(R.string.title_activity_create_list);
        }

        // If this activity starts in landscape, pop back out, it's not supposed to run that way
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            getEZApplication().setTwoPane(true);
            finish();
        }

        Bundle bundle = savedInstanceState != null ? savedInstanceState : getIntent().getExtras();

        if (bundle != null && bundle.containsKey(ShoppingListDetailFragment.ARG_LIST_ID)) {
            ((ShoppingListEditFragment) getSupportFragmentManager().findFragmentById(R.id.edit_fragment))
                    .setShoppingListId(bundle.getString(ShoppingListDetailFragment.ARG_LIST_ID));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.shoppinglist_edit_menu, menu);

        // Make the menu icon white
        Drawable icon = menu.findItem(R.id.action_delete).getIcon();

        if (icon != null) {
            icon.mutate();
            icon.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_delete) {
            ((ShoppingListEditFragment) getSupportFragmentManager().findFragmentById(R.id.edit_fragment)).deleteList();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
