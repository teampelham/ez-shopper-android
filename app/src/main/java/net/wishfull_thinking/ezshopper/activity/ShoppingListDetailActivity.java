package net.wishfull_thinking.ezshopper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import net.wishfull_thinking.ezshopper.R;
import net.wishfull_thinking.ezshopper.fragment.ShoppingListDetailFragment;

/**
 * An activity representing a single ShoppingList detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ShoppingListListActivity}.
 */
public class ShoppingListDetailActivity extends BaseActivity {

    private static final String TAG_LIST_DISPLAY = "list_display";
    public static final String TAG_DETAIL_DISPLAY = "detail_display";

    private String mListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoppinglist_detail);

        showUpAsHome();

        // Create the detail fragment and add it to the activity
        // using a fragment transaction.
        Bundle bundle = savedInstanceState != null ? savedInstanceState : getIntent().getExtras();
        Bundle arguments = new Bundle();

        if (bundle != null) {
            mListId = bundle.getString(ShoppingListDetailFragment.ARG_LIST_ID);
            arguments.putString(ShoppingListDetailFragment.ARG_LIST_ID, mListId);
        }

        ShoppingListDetailFragment fragment = new ShoppingListDetailFragment();
        fragment.setArguments(arguments);
        getFragmentManager().beginTransaction()
                .replace(R.id.shoppinglist_detail_container, fragment, TAG_LIST_DISPLAY)
                .commit();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        if (mListId != null) {
            bundle.putString(ShoppingListDetailFragment.ARG_LIST_ID, mListId);
        }

        super.onSaveInstanceState(bundle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, ShoppingListListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
