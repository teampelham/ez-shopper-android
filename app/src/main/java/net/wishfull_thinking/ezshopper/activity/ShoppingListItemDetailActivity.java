package net.wishfull_thinking.ezshopper.activity;

import android.os.Bundle;
import android.view.MenuItem;

import net.wishfull_thinking.ezshopper.R;
import net.wishfull_thinking.ezshopper.fragment.ShoppingListDetailFragment;
import net.wishfull_thinking.ezshopper.fragment.ShoppingListItemDetailFragment;

public class ShoppingListItemDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list_item_detail);

        showUpAsHome();

        Bundle bundle = savedInstanceState != null ? savedInstanceState : getIntent().getExtras();

        if (bundle != null && bundle.containsKey(ShoppingListDetailFragment.ARG_LIST_ID)) {
            ((ShoppingListItemDetailFragment) getSupportFragmentManager().findFragmentById(R.id.detail_fragment))
                    .setShoppingListItemId(bundle.getString(ShoppingListDetailFragment.ARG_LIST_ID),
                            bundle.getString(ShoppingListDetailFragment.ARG_ITEM_ID));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
