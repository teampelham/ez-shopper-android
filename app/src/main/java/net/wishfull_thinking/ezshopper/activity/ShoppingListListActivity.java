package net.wishfull_thinking.ezshopper.activity;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.OvershootInterpolator;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import net.wishfull_thinking.ezshopper.EZShopperApplication;
import net.wishfull_thinking.ezshopper.R;
import net.wishfull_thinking.ezshopper.Utils;
import net.wishfull_thinking.ezshopper.adapter.ShoppingListAdapter;
import net.wishfull_thinking.ezshopper.fragment.ShoppingListDetailFragment;
import net.wishfull_thinking.ezshopper.fragment.ShoppingListEditFragment;
import net.wishfull_thinking.ezshopper.viewmodel.ShoppingListActivityViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * An activity representing a list of ShoppingLists.
 */
@SuppressWarnings("WeakerAccess")
public class ShoppingListListActivity extends BaseActivity
    implements ShoppingListAdapter.OnShoppingListSelectedListener {

    private static final String ADD_LIST_DIALOG_TAG = "add_list_dialog";
    private static final int ANIM_DURATION_TOOLBAR = 300;
    private static final int ANIM_DURATION_FAB = 400;
    private static final int RC_SIGN_IN = 123;


    @BindView(R.id.fab)
    FloatingActionButton mFab;

    @BindView(R.id.shoppinglist_list)
    RecyclerView mShoppingListsRecycler;

    @BindView(R.id.view_empty)
    ViewGroup mEmptyView;

    private FirebaseFirestore mFirestore;
    private FirebaseUser mUser;
    private ShoppingListActivityViewModel mViewModel;
    private ShoppingListAdapter mAdapter = null;
    private String mListId;
    private boolean mTwoPane;
    private boolean pendingIntroAnimation;


    @SuppressLint("hardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoppinglist_list);

        if (mToolbar != null) {
            mToolbar.setTitle(getTitle());
        }

        // view model
        mViewModel = ViewModelProviders.of(this).get(ShoppingListActivityViewModel.class);

        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true);

        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        // Determine screen size
        if (findViewById(R.id.shoppinglist_detail_container) != null) {
            mTwoPane = true;
            getEZApplication().setTwoPane(mTwoPane);
        }

        // Handle FAB visibility
        if (mTwoPane) {
            mFab.setVisibility(View.GONE);
        } else {
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startAddNewShoppingList();
                }
            });
        }

        // Trigger intro animation
        if(savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // Start sign in if necessary
        if (shouldStartSignIn()) {
            startSignIn();
            return;
        }

        if (mViewModel.getIsSigningIn()) {
            return;
        }

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        setupRecyclerView();
        setupGlobalData();
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        // Kick off the intro animation
        if(pendingIntroAnimation) {
            pendingIntroAnimation = false;
            startIntroAnimation();
        }

        // Show the add-new menu for dual pane, since FAB is hidden
        if (mTwoPane) {
            menu.findItem(R.id.action_add_new).setVisible(true);
        }

        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        if (mListId != null) {
            state.putString(ShoppingListDetailFragment.ARG_LIST_ID, mListId);
        }
        super.onSaveInstanceState(state);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if Result came from the sign-in activity
        if(requestCode == RC_SIGN_IN) {
            mViewModel.setIsSigningIn(false);

            if(resultCode != RESULT_OK) {
                startSignIn();
            } else {
                onStart();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_add_new) {
            startAddNewShoppingList();
            return true;
        }
        else if (id == R.id.action_sign_out) {
            AuthUI.getInstance().signOut(this);
            startSignIn();
        }

        return super.onOptionsItemSelected(item);
    }

    private void startAddNewShoppingList() {
        if (mTwoPane) {
            ShoppingListEditFragment fragment = new ShoppingListEditFragment();
            fragment.show(getSupportFragmentManager(), ADD_LIST_DIALOG_TAG);
        } else {
            Intent intent = new Intent(ShoppingListListActivity.this, ShoppingListEditActivity.class);
            startActivity(intent);
        }
    }

    private void startIntroAnimation() {
        mFab.setTranslationY(2 * getResources().getDimensionPixelOffset(R.dimen.btn_fab_size));

        int actionbarSize = Utils.dpToPx(56);
        if (mToolbar != null) {
            mToolbar.setTranslationY(-actionbarSize);

            mToolbar.animate()
                    .translationY(0)
                    .setDuration(ANIM_DURATION_TOOLBAR)
                    .setStartDelay(300);
        }

        mFab.animate()
            .translationY(0)
            .setInterpolator(new OvershootInterpolator(1.0F))
            .setDuration(ANIM_DURATION_FAB)
            .setStartDelay(400)
            .setListener(new Animator.AnimatorListener() {

                @Override
                public void onAnimationEnd(Animator animation, boolean isReverse) {
                    runLayoutAnimation();
                }

                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }
            })
            .start();
    }

    private void setupRecyclerView() {
        Query query = mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                .collection(EZShopperApplication.SHOPPING_LISTS)
                .orderBy(EZShopperApplication.LIST_NAME);

        mAdapter = new ShoppingListAdapter(this, query, this) {
            @Override
            protected void onDataChanged() {
                // Show/hide content if the query returns empty
                if (getItemCount() == 0) {
                    mShoppingListsRecycler.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                } else {
                    mShoppingListsRecycler.setVisibility(View.VISIBLE);
                    mEmptyView.setVisibility(View.GONE);
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {
                // Show a snackbar for errors
                Snackbar.make(findViewById(android.R.id.content), "Error: check logs for info", Snackbar.LENGTH_LONG).show();
            }
        };

        mShoppingListsRecycler.setLayoutManager(new LinearLayoutManager(this));
        mShoppingListsRecycler.setAdapter(mAdapter);
    }

    private void runLayoutAnimation() {
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(mShoppingListsRecycler.getContext(), R.anim.layout_animation_fall_down);
        mShoppingListsRecycler.setLayoutAnimation(animation);
        mAdapter.notifyDataSetChanged();
        mShoppingListsRecycler.scheduleLayoutAnimation();
    }

    @Override
    public void onShoppingListSelected(DocumentSnapshot shoppingList) {
        mListId = shoppingList.getId();

        if(mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putString(ShoppingListDetailFragment.ARG_LIST_ID, mListId);

            ShoppingListDetailFragment fragment = new ShoppingListDetailFragment();
            fragment.setArguments(arguments);
            getFragmentManager().beginTransaction()
                    .replace(R.id.shoppinglist_detail_container, fragment, ShoppingListDetailActivity.TAG_DETAIL_DISPLAY)
                    .commit();
        } else {
            Intent intent = new Intent(this, ShoppingListDetailActivity.class);
            intent.putExtra(ShoppingListDetailFragment.ARG_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @Override
    public void onShoppingListEdited(DocumentSnapshot shoppingList) {
        String listId = shoppingList.getId();

        if(mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putString(ShoppingListDetailFragment.ARG_LIST_ID, listId);

            ShoppingListEditFragment fragment = new ShoppingListEditFragment();
            fragment.setArguments(arguments);
            fragment.show(getSupportFragmentManager(), ADD_LIST_DIALOG_TAG);
        } else {
            Intent intent = new Intent(this, ShoppingListEditActivity.class);
            intent.putExtra(ShoppingListDetailFragment.ARG_LIST_ID, listId);
            startActivity(intent);
        }
    }

    private boolean shouldStartSignIn() {
        return (!mViewModel.getIsSigningIn() && FirebaseAuth.getInstance().getCurrentUser() == null);
    }

    private void startSignIn() {
        // Sign in with FirebaseUI
        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                .setIsSmartLockEnabled(false)
                .build();

        startActivityForResult(intent, RC_SIGN_IN);
        mViewModel.setIsSigningIn(true);
    }

    private void setupGlobalData() {
        final EZShopperApplication app = (EZShopperApplication) getApplication();

        mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful() && task.getResult().exists()) {
                            DocumentSnapshot document = task.getResult();
                            app.initializeGlobalData(document);
                        } else {
                            initializeGlobalData();
                        }
                    }
                });
    }

    private void initializeGlobalData() {
        Map<String, Object> user = new HashMap<>();
        user.put(EZShopperApplication.CATEGORY_LIST, new ArrayList<String>());
        user.put(EZShopperApplication.PRODUCT_LIST, new ArrayList<String>());
        user.put(EZShopperApplication.QUANTITY_TYPE_LIST, new ArrayList<String>());

        mFirestore.collection(EZShopperApplication.USERS).document(mUser.getUid())
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        setupGlobalData();
                    }
                });
    }
}
