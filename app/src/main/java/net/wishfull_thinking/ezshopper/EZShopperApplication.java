package net.wishfull_thinking.ezshopper;

import android.app.Application;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dennis on 10/28/17.
 * To further the cause of Awesomeness
 */

public class EZShopperApplication extends Application {

    public static final String USERS = "users";
    public static final String QUANTITY_TYPE_LIST = "quantity_types";
    public static final String CATEGORY_LIST = "category_list";
    public static final String PRODUCT_LIST = "product_list";
    public static final String SHOPPING_LISTS = "lists";
    public static final String LIST_NAME = "name";
    public static final String CATEGORY = "category";
    public static final String DONE = "done";
    public static final String SHOPPING_LIST_ITEMS = "listItems";

    private boolean mTwoPane;
    private DocumentReference mDocumentReference;
    private List<String> mQuantityTypes;
    private List<String> mProducts;
    private List<String> mCategories;
    private List<String> mShoppingListList;
    private Map<String, String> mShoppingListNameMap;
    private Map<String, String> mShoppingListIdMap;
    private ArrayAdapter<String> mProductsAdapter;
    private ArrayAdapter<String> mCategoriesAdapter;
    private ArrayAdapter<String> mQuantityTypesAdapter;
    private ArrayAdapter<String> mShoppingListAdapter;

    public boolean getTwoPane() {
        return mTwoPane;
    }

    public void setTwoPane(boolean twoPane) {
        mTwoPane = twoPane;
    }

    @SuppressWarnings("unchecked")
    public void initializeGlobalData(DocumentSnapshot documentSnapshot) {
        List<String> quantityTypes = (List<String>) documentSnapshot.get(QUANTITY_TYPE_LIST);
        List<String> categories = (List<String>) documentSnapshot.get(CATEGORY_LIST);
        List<String> products = (List<String>) documentSnapshot.get(PRODUCT_LIST);
        mDocumentReference = documentSnapshot.getReference();

        initializeProducts(products);
        initializeCategories(categories);
        initializeQuantityTypes(quantityTypes);
        initializeShoppingLists();
    }

    private void initializeShoppingLists() {
        if (mDocumentReference == null) {
            return;
        }

        mDocumentReference.collection(SHOPPING_LISTS).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mShoppingListNameMap = new HashMap<>();
                            mShoppingListIdMap = new HashMap<>();
                            mShoppingListList = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                mShoppingListNameMap.put(document.getString("name"), document.getId());
                                mShoppingListIdMap.put(document.getId(), document.getString("name"));
                                mShoppingListList.add(document.getString("name"));
                            }
                            mShoppingListAdapter = createAdapter(mShoppingListList);
                        }
                    }
                });
    }

    private void initializeProducts(List<String> products) {
        if (products != null) {
            mProducts = products;
        } else {
            mProducts = new ArrayList<>();
        }
        mProductsAdapter = createAdapter(mProducts);
    }

    private void initializeCategories(List<String> categories) {
        if (categories != null) {
            mCategories = categories;
        } else {
            mCategories = new ArrayList<>();
        }
        mCategoriesAdapter = createAdapter(mCategories);
    }

    private void initializeQuantityTypes(List<String> quantityTypes) {
        if (quantityTypes != null) {
            mQuantityTypes = quantityTypes;
        } else {
            mQuantityTypes = new ArrayList<>();
        }
        mQuantityTypesAdapter = createAdapter(mQuantityTypes);
    }

    public void updateListData(String product, String category, String quantityType) {
        addQuantityType(quantityType);
        addCategory(category);
        addProduct(product);

        updateDocument();
    }

    private void addQuantityType(String quantityType) {
        if (quantityType == null || mQuantityTypes.contains(quantityType)) {
            return;
        }

        mQuantityTypes.add(quantityType);

        mQuantityTypesAdapter.notifyDataSetChanged();
    }

    private void addCategory(String category) {
        if (category == null || mCategories.contains(category)) {
            return;
        }

        mCategories.add(category);

        mCategoriesAdapter.notifyDataSetChanged();
    }

    private void addProduct(String product) {
        if (product == null || mProducts.contains(product)) {
            return;
        }

        mProducts.add(product);

        mProductsAdapter.notifyDataSetChanged();
    }

    public String getShoppingListId(String listName) {
        return mShoppingListNameMap.get(listName);
    }

    public String getShoppingListName(String listId) {
        return mShoppingListIdMap.get(listId);
    }

    public ArrayAdapter<String> getCategoriesAdapter() {
        return mCategoriesAdapter;
    }

    public ArrayAdapter<String> getProductsAdapter() {
        return mProductsAdapter;
    }

    public ArrayAdapter<String> getQuantityTypesAdapter() {
        return mQuantityTypesAdapter;
    }

    public ArrayAdapter<String> getShoppingListAdapter() {
        return mShoppingListAdapter;
    }

    private void updateDocument() {
        WriteBatch batch = FirebaseFirestore.getInstance().batch();

        batch.update(mDocumentReference, PRODUCT_LIST, mProducts);
        batch.update(mDocumentReference, CATEGORY_LIST, mCategories);
        batch.update(mDocumentReference, QUANTITY_TYPE_LIST, mQuantityTypes);

        batch.commit();
    }

    @NonNull
    private ArrayAdapter<String> createAdapter(List<String> data) {
        return new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data);
    }
}
