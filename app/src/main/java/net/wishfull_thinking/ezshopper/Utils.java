package net.wishfull_thinking.ezshopper;

import android.content.res.Resources;

/**
 * Created by dennis on 7/4/17.
 * Does utility thingies
 */

public class Utils {

    public static int dpToPx(@SuppressWarnings("SameParameterValue") int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static boolean isBlank(String text) {
        return (text == null || text.isEmpty());
    }
}
