package net.wishfull_thinking.ezshopper.adapter;

/**
 * Created by dennis on 5/20/17.
 * View model for a shopping list item
 */

@SuppressWarnings("unused")
public class ShoppingListItem {
    private String mName;
    private String mCategory;
    private String mQuantity;
    private String mQuantityType;
    private boolean mDone;
    private boolean mHasChanges;

    public ShoppingListItem() {    }

    public ShoppingListItem(String name, String quantity, String quantityType, String category, boolean done) {
        this(name, quantity, quantityType, category);

        mDone = done;
    }

    public ShoppingListItem(String name, String quantity, String quantityType, String category) {
        mName = name;
        mCategory = category;
        mQuantity = quantity;
        mQuantityType = quantityType;
        mDone = false;
        mHasChanges = false;
    }

    public boolean isValid() {
        return mName != null && mName.length() > 0;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        if (name != null && !name.equals(mName)) {
            mName = name;
            mHasChanges = true;
        }
    }

    public String getCategory() {
        return mCategory;
    }

    void setCategory(String category) {
        if (category != null && !category.equals(mCategory)) {
            mCategory = category;
            mHasChanges = true;
        }
    }

    public String getQuantity() {
        return mQuantity;
    }

    void setQuantity(String quantity) {
        if (quantity != null && !quantity.equals(mQuantity)) {
            mQuantity = quantity;
            mHasChanges = true;
        }
    }

    public String getQuantityType() {
        return mQuantityType;
    }

    void setQuantityType(String quantityType) {
        if (quantityType != null && !quantityType.equals(mQuantityType)) {
            mQuantityType = quantityType;
            mHasChanges = true;
        }
    }

    void setDone(boolean done) {
        if (done != mDone) {
            mDone = done;
            mHasChanges = true;
        }
    }

    boolean getDone() {
        return mDone;
    }

    public boolean hasChanges() {
        return mHasChanges;
    }

    public void resetChanges() {
        mHasChanges = false;
    }
}
