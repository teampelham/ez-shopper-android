package net.wishfull_thinking.ezshopper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

import net.wishfull_thinking.ezshopper.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dennis on 10/15/17.
 * Adapt my shopping lists!
 */

public class ShoppingListAdapter extends FirestoreAdapter<ShoppingListAdapter.ViewHolder> {

    public interface OnShoppingListSelectedListener {

        void onShoppingListSelected(DocumentSnapshot shoppingList);
        void onShoppingListEdited(DocumentSnapshot shoppingList);
    }

    private final OnShoppingListSelectedListener mListener;

    public ShoppingListAdapter(Context context, Query query, OnShoppingListSelectedListener listener) {
        super(context, query);
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.viewholder_shoppinglist, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(getSnapshot(position), mListener);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.list_name)
        TextView listNameView;

        @BindView(R.id.list_descr)
        TextView listDescrView;

        @BindView(R.id.list_edit)
        FrameLayout editButtonContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final DocumentSnapshot snapshot, final OnShoppingListSelectedListener listener) {

            ShoppingList shoppingList = snapshot.toObject(ShoppingList.class);

            listNameView.setText(shoppingList.getName());
            listDescrView.setText(shoppingList.getDescr());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onShoppingListSelected(snapshot);
                    }
                }
            });

            editButtonContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onShoppingListEdited(snapshot);
                    }
                }
            });
        }
    }
}
