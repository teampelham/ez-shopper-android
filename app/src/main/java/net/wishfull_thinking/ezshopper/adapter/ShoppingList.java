package net.wishfull_thinking.ezshopper.adapter;

/**
 * Created by Me on 2/21/2017.
 * View model for a Shopping List
 */

@SuppressWarnings("unused")
public class ShoppingList {
    private String mName;
    private String mDescr;
    private boolean mHasChanges;

    public ShoppingList() {
        mHasChanges = false;
    }


    public String getName() {
        return mName;
    }

    public void setName(String name) {
        if (name != null && !name.equals(mName)) {
            mName = name;
            mHasChanges = true;
        }
    }

    public String getDescr() {
        return mDescr;
    }

    public void setDescr(String descr) {
        if (descr != null && !descr.equals(mDescr)) {
            mDescr = descr;
            mHasChanges = true;
        }
    }

    public boolean isValid() {
        return mName != null && mName.length() > 0;
    }

    public boolean hasChanges() {
        return mHasChanges;
    }

    public void resetChanges() {
        mHasChanges = false;
    }
}
