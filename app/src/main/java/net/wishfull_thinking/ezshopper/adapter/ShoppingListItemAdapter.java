package net.wishfull_thinking.ezshopper.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

import net.wishfull_thinking.ezshopper.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dennis on 10/15/17.
 * Does things
 */

public class ShoppingListItemAdapter extends FirestoreAdapter<ShoppingListItemAdapter.ViewHolder> {

    public interface OnShoppingListItemSelectedListener {

        void onShoppingListItemSelected(DocumentSnapshot shoppingList);
        void onShoppingListItemDone(DocumentSnapshot shoppingList, View view);
        boolean shouldShowDoneItem(DocumentSnapshot snapshot);
    }

    private final OnShoppingListItemSelectedListener mListener;

    public ShoppingListItemAdapter(Context context, Query query, OnShoppingListItemSelectedListener listener) {
        super(context, query);
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.viewholder_shoppinglist_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DocumentSnapshot snapshot = getSnapshot(position);
        if (!snapshot.getBoolean("done") || mListener.shouldShowDoneItem(snapshot)) {
            holder.bind(snapshot, mListener);
        } else {
            holder.hideView();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_done)
        AppCompatCheckBox doneCheckbox;

        @BindView(R.id.item_name)
        TextView nameView;

        @BindView(R.id.item_quantity)
        TextView quantityView;

        private final LinearLayout.LayoutParams params;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        public void bind(final DocumentSnapshot snapshot, final OnShoppingListItemSelectedListener listener) {

            ShoppingListItem shoppingListItem = snapshot.toObject(ShoppingListItem.class);
            String quantity = shoppingListItem.getQuantity();

            if (shoppingListItem.getQuantityType() != null) {
                quantity += " " + shoppingListItem.getQuantityType();
            }

            nameView.setText(shoppingListItem.getName());
            quantityView.setText(quantity);
            doneCheckbox.setChecked(shoppingListItem.getDone());

            doneCheckbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onShoppingListItemDone(snapshot, view);
                    }
                }
            });

            if(shoppingListItem.getDone()) {
                nameView.setTextColor(Color.GRAY);
                nameView.setPaintFlags(nameView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            else {
                nameView.setTextColor(Color.BLACK);
                nameView.setPaintFlags(nameView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onShoppingListItemSelected(snapshot);
                    }
                }
            });

            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            itemView.setLayoutParams(params);
        }

        public void hideView() {
            params.height = 0;
            //itemView.setLayoutParams(params); //This One.
            itemView.setLayoutParams(params);   //Or This one.
        }
    }
}
