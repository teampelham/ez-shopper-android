package net.wishfull_thinking.ezshopper.viewmodel;

import android.arch.lifecycle.ViewModel;

/**
 * Created by dennis on 10/15/17.
 * View my model
 */

public class ShoppingListActivityViewModel extends ViewModel {

    private boolean mIsSigningIn;

    public ShoppingListActivityViewModel() {
        mIsSigningIn = false;
    }

    public boolean getIsSigningIn() {
        return mIsSigningIn;
    }

    public void setIsSigningIn(boolean isSigningIn) {
        mIsSigningIn = isSigningIn;
    }
}
